package com.ettud.itis_2020_w3

import android.content.Context
import android.media.MediaPlayer
import android.widget.MediaController
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.SurfaceHolder
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_coub.*

class CoubFragment : Fragment() {
    private lateinit var coubItem: CoubLvItem
    private var audioPlayer : MediaPlayer? = null
    private var videoPlayer : MediaPlayer? = null
    private var players: List<MediaPlayer>
        get() = arrayOf(audioPlayer, videoPlayer).filterNotNull().map { it }
        set(value) = throw Exception()

    companion object {

        fun newInstance(): CoubFragment {
            return CoubFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_coub, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (arguments != null) {
            coubItem = arguments?.getSerializable("coub") as CoubLvItem
            coubTitle.text = coubItem.title
            coubLikesCount.text = "L " + coubItem.likesCount
            coubViewsCount.text = "V " + coubItem.viewsCount

            if(activity != null) {
                val audioUri = Uri.parse(coubItem.audioUrl)
                val videoUri = Uri.parse(coubItem.videoUrl)
                if(audioUri != null || videoUri != null){
                    if(audioUri != null) {
                        audioPlayer = MediaPlayer.create(activity, Uri.parse(coubItem.audioUrl))
                        audioPlayer?.isLooping = true
                    }
                    if(videoUri != null) {
                        videoPlayer = MediaPlayer.create(activity, Uri.parse(coubItem.videoUrl))
                        videoPlayer?.isLooping = true
                    }
                    coubView.holder.addCallback(object: SurfaceHolder.Callback {
                        override fun surfaceChanged(holder: SurfaceHolder, format: Int,
                                                    width: Int, height: Int) {
                        }

                        override fun surfaceCreated(holder: SurfaceHolder) {
                            videoPlayer?.setDisplay(coubView.holder)
                            startCoub()
                        }

                        override fun surfaceDestroyed(holder: SurfaceHolder) {
                            stopCoub()
                        }
                    })
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        startCoub()
    }

    override fun onStop() {
        super.onStop()
        stopCoub()
    }

    override fun onPause() {
        super.onPause()
        pauseCoub()
    }

    override fun onResume(){
        super.onResume()
        resumeCoub()
    }

    private fun startCoub(){
        players.forEach{it.start()}
    }

    private fun stopCoub(){
        players.forEach{it.stop()}
    }

    private fun pauseCoub(){
        players.forEach{it.pause()}
    }

    private fun resumeCoub(){
        players.forEach{it.start()}
    }
}
