package com.ettud.itis_2020_w3

import java.io.Serializable

data class CoubLvItem(
    val id: Int,
    val title: String,
    val likesCount: Int,
    val viewsCount: Int,
    val videoUrl: String,
    val audioUrl: String
) : Serializable