package com.ettud.itis_2020_w3

import TimelineResponse
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.ettud.itis_2020_w3.CoubPresenter.CoubPresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_coubs.*

class CoubsFragment : Fragment() {
    companion object {

        fun newInstance(): CoubsFragment {
            return CoubsFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View?  {
        return inflater.inflate(R.layout.fragment_coubs, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?){
        super.onActivityCreated(savedInstanceState)
        CoubPresenter().getHottest().observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(this::handleResponse, this::handleError)
    }

    private fun handleResponse(result: TimelineResponse){
        awaitingText.visibility = View.INVISIBLE
        errorText.visibility = View.INVISIBLE
        if(activity != null) {
            val activity : FragmentActivity = activity!!
            val coubLvAdapter = CoubLvAdapter(
                activity,
                ArrayList<CoubLvItem>(result.coubs.map {
                    CoubLvItem(it.id, it.title, 0, 0, it.files.mobileFile.videoUrl, it.files.mobileFile.audioUrls[0])
                }))
            lvCoubs.adapter = coubLvAdapter
            lvCoubs.setOnItemClickListener { _, _, position, _ ->
                val selectedCoub = coubLvAdapter.getItem(position)
                val newFragment = CoubFragment.newInstance()
                val arguments = Bundle()
                arguments.putSerializable("coub", selectedCoub)
                newFragment.arguments = arguments
                val manager = activity.supportFragmentManager
                manager.beginTransaction().replace(R.id.main_fragment, newFragment, "coub" + selectedCoub.id)
                    .addToBackStack("coub")
                    .commit()
            }
            lvCoubs.visibility = View.VISIBLE
        }
        else{
            handleError(Throwable("Activity is down"))
        }
    }

    private fun handleError(error: Throwable){
        awaitingText.visibility = View.INVISIBLE
        errorText.visibility = View.VISIBLE
        lvCoubs.visibility = View.INVISIBLE
        Toast.makeText(activity, error.message, Toast.LENGTH_LONG).show()
    }
}
