package com.ettud.itis_2020_w3

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.core.content.ContextCompat.getSystemService
import kotlinx.android.synthetic.main.coub_lv_item.view.*

class CoubLvAdapter(context: Context, items: ArrayList<CoubLvItem>) : BaseAdapter() {
    private val coubs : ArrayList<CoubLvItem> = items
    private val ctx : Context = context
    private val lInflater : LayoutInflater

    init {
        lInflater = LayoutInflater.from(ctx)
    }

    override fun getCount(): Int {
        return coubs.size
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        // Get view for row item
        val rowView = lInflater.inflate(R.layout.coub_lv_item, parent, false)
        val coubItem = this.getItem(position)
        rowView.coubTitle.text = coubItem.title
        rowView.coubLikesCount.text = "L " + coubItem.likesCount
        rowView.coubViewsCount.text = "V " + coubItem.viewsCount
        return rowView
    }

    override fun getItem(position: Int): CoubLvItem {
        return coubs[position]
    }

    override fun getItemId(position: Int): Long {
        return (coubs[position].id as Number).toLong()
    }
}