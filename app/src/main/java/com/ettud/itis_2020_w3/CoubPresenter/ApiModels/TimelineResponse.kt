import com.google.gson.annotations.SerializedName

//https://coub.com/dev/docs/Coub+API/Timelines
data class TimelineResponse(
    @SerializedName("coubs")
    val coubs: Array<CoubResponse>
)