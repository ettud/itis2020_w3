import com.google.gson.annotations.SerializedName

//https://coub.com/dev/docs/data+structures/coub+big+json
data class CoubResponse(
    @SerializedName("id")
    val id: Int,
    @SerializedName("title")
    val title: String,
    @SerializedName("file_versions")
    val files: CoubFiles,
    @SerializedName("likes_counts")
    val likesCount: Int,
    @SerializedName("views_count")
    val viewsCount: Int,
    @SerializedName("tags")
    val tags: Array<CoubTag>
)