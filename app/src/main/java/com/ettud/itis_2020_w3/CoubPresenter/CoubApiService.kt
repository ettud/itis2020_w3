package com.ettud.itis_2020_w3.CoubPresenter

import TimelineResponse
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface CoubApiService {

    @GET("v2/timeline/hot?page=1&per_page=100")
    fun getHotTimeline(): Observable<TimelineResponse>

    /**
     * Factory class for convenient creation of the Api Service interface
     */
    object Factory {

        fun create(): CoubApiService {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://coub.com/api/")
                .build()

            return retrofit.create(CoubApiService::class.java)
        }
    }
}