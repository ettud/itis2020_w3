package com.ettud.itis_2020_w3.CoubPresenter

import TimelineResponse
import io.reactivex.Observable

class CoubPresenter {
    private val apiService: CoubApiService

    init {
        this.apiService = CoubApiService.Factory.create()
    }

    fun getHottest() : Observable<TimelineResponse> {
        return apiService.getHotTimeline()
    }
}