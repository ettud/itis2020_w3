import com.google.gson.annotations.SerializedName

data class CoubTag(
    @SerializedName("id")
    val id: Int,
    @SerializedName("title")
    val title: String,
    @SerializedName("value")
    val value: String
)