import com.google.gson.annotations.SerializedName

data class CoubMobileFile(
    @SerializedName("video")
    val videoUrl: String,
    @SerializedName("audio")
    val audioUrls: Array<String>
)