import com.google.gson.annotations.SerializedName

data class CoubFiles(
    @SerializedName("mobile")
    val mobileFile: CoubMobileFile
)